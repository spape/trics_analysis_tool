from setuptools import setup


setup(
    name='trics_analysis_scripts',
    version='1.09',
    author='Sebastian Pape',
    author_email='sebastian.pape@cern.ch',
    packages=[
        'trics_analysis_scripts',
    ],
    install_requires=[
        'argparse',
        'numpy',
        'scipy',
        'matplotlib',
        'uncertainties'
    ],
    entry_points={
        'console_scripts': [
            'trics_CollectedCharge = trics_analysis_scripts.multigraph_CollectedCharge:main',
            'trics_DriftVelocity = trics_analysis_scripts.multigraph_DriftVelocity:main',
            'trics_Multigraph = trics_analysis_scripts.multigraph:main',
            'trics_Power = trics_analysis_scripts.multigraph_power:main',
            'trics_zScan = trics_analysis_scripts.zScan:main',
            'trics_2D = trics_analysis_scripts.Graph2D:main',
            'trics_TiltCorrection = trics_analysis_scripts.tilt_correction:main',
            'trics_Stability = trics_analysis_scripts.stability:main',
            'trics_KnifeEdge = trics_analysis_scripts.KnifeEdge:main'
        ]
    }
)
