"""
Script to create plots that are optimized to present a quantity over a long time periode, e.g. Laser power vs time or Temperature vs time.
Developer: Sebastian Pape
E-Mail: sebastian.pape@cern.ch


This script is executable from the terminal with 'trics_Stability'.
Required arguments for the parser:
    filename: name of the root file
	Quantity: Quantity of the stability plot (Implemented options: T, LP)
Optional arguments for the parser:
	-signal: Plot signal inside the stability plot (default True)
	-type: n or p type DUT (default is p type)
"""

import argparse
import sys
import os
import ROOT as rt
import numpy as np
from . import utils

"""
Argument definitons for the parser.
"""
parser = argparse.ArgumentParser()
parser.add_argument('filename', help='name of the root file')
parser.add_argument('-q', help='Quantity of the stability plot (Implemented options: T, LP)', type=str)
parser.add_argument('-signal', help='Plot signal inside the stability plot (default True)', type=str, default=True)
parser.add_argument('-type', help='n or p type DUT (default is p type)', default="p")



def main():
	"""
	Create the folder plots, if it does not exist.
	"""
	if not os.path.isdir("plots"):
		os.mkdir("plots")

	"""
	Get all arguments from the parser.
	"""
	args = parser.parse_args()
	filename = args.filename
	measurement_name = filename.split(".")[0]
	y_axis = args.q
	if y_axis.lower() in ["t", "temp", "temperature"]:
		y_axis = "Temp"
	elif y_axis.lower() in ["lp", "laserpower", "laser", "l"]:
		y_axis = "LPower"
	signal = utils.str2bool(args.signal)
	dut_type = args.type
	if dut_type == "p":
		signal_amp = 'Vmin'
	else:
		signal_amp = 'Vmax'


	"""
	Read in root file from filename and create the ch0 tree.
	"""
	f = rt.TFile(filename)
	tree = f.Get('ch0')

	"""
	Create a TCanvas with size 1000px times 600px, and a TMultigraph which is later filled with the TGraphs.
	The title of the multigraph is set to the measurements name.
	"""
	c1 = rt.TCanvas("c1", "c1", 1000, 600)

	"""
	Create TGraphs for all voltages with the options defined by the parsed arguments.
	"""
	#try:
	y_axis_array, Vmin, Vmax = utils.GetBranchAsArray(filename, "ch0", [y_axis, "Vmin", "Vmax"])

	if np.max(np.abs(Vmin)) > np.max(Vmax):
		signal_norm_factor = np.mean(Vmin)
		print(signal_norm_factor)
	else:
		signal_norm_factor = np.mean(Vmax)
	y_axis_norm_factor = np.mean(y_axis_array)
	tree.Draw(f"{y_axis}/{y_axis_norm_factor}:utc.Convert()", "", "goff")
	graph = rt.TGraph(tree.GetSelectedRows(), tree.GetV2(), tree.GetV1())
	graph.SetLineWidth(3)
	graph.SetLineColor(2)
	graph.SetMarkerSize(0)
	graph.Draw("")
	if signal:
		rt.gStyle.SetPalette(87)
		tree.Draw(f"{signal_amp}/{signal_norm_factor}:utc.Convert()", "", "goff")
		graph_signal = rt.TGraph(tree.GetSelectedRows(), tree.GetV2(), tree.GetV1())
		graph_signal.SetLineWidth(3)
		graph_signal.SetLineColor(4)
		graph_signal.SetMarkerSize(0)
		graph_signal.SetName(f"DUT")
		graph_signal.SetTitle(f"DUT")
		graph_signal.Draw("same")
	c1.SetBottomMargin(0.15)
	graph.SetName(f"{y_axis}")
	graph.SetTitle(f"{y_axis}")
	graph.GetYaxis().SetTitle(f"{y_axis}")
	graph.GetXaxis().SetTimeDisplay(1)
	graph.GetXaxis().SetLabelOffset(0.03)
	graph.GetXaxis().SetTitleOffset(1.7)
	graph.GetXaxis().SetTimeFormat("#splitline{%d/%m/%y}{%H:%M}")
	graph.GetXaxis().SetTimeOffset(7200, "GMT")
	graph.GetXaxis().SetTitle("Date")
	c1.BuildLegend(0.8, 0.8, 0.89, 0.89)
	rt.gPad.SetGrid(1)
	rt.gPad.Update()

	"""
	Save the plots as a png and a root file.
	"""
	c1.SaveAs(f"plots/{measurement_name}_{y_axis}.png")
	#except:
	#	sys.exit(f"Option {y_axis} for flag -q not known.\nEnter trics_stability --help for help.")


if __name__ == '__main__':
	main()
