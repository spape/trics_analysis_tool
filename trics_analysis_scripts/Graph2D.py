"""
Script to create two dimensional plots from a TPA-TCT measurement, taken with the TCT- setup at CERN and
analized with the TRICS framework.
Developer: Sebastian Pape
E-Mail: sebastian.pape@cern.ch


This script is executable from the terminal with 'trics_2D'.
Required arguments for the parser:
    filename: name of the root file
Optional arguments for the parser:
    -v: voltages as -v start -v stop -v steps
   	-ev: exclude a given voltage
    -o: appendix for the output file
    -x: Quantity to plot on the x-axis (default x)
    -y: Quantity to plot on the y-axis (default y)
    -z: Quantity to plot on the z-axis (implemented options: Charge, VD; default Charge)
    -npx: Binning size of x-axis (default length of X value array)
    -npy: Binning size of y-axis (default length of Y value array)
    -c: Condition for the plot
    -tcoll: ONLY FOR CHARGE COLLECTION: Time in ns for which the charge should be collected (default is 10ns)
    -tleft: ONLY FOR CHARGE COLLECTION AND DRIFT VELOCITY: Choose between tleft and atleft (default tleft)
    -type: n or p type DUT (default is p type)
    -norm: norm by reference laser power, only available for Charge and VD (default is False)
    -z_t_off: Offset for the Z axis title (default 1.8)
    -rr: Real aspect ratio of the x and y axises (default True)
    -inSi: Multiply z with a hard coded factor that converts it from "in air" to "in silicon" (default is False)
    -contour: Draw a contour line around 0 (default is False)
"""

import argparse
import os
import sys
import ROOT as rt
import numpy as np
from . import utils

rt.gROOT.SetBatch(True)

"""
Argument definitons for the parser.
"""
parser = argparse.ArgumentParser()
parser.add_argument('filename', help='name of the root file')
parser.add_argument('-v', action='append', help='voltages as -v start -v stop -v steps')
parser.add_argument('-ev', action='append', help='exclude a given voltage')
parser.add_argument('-o', help='appendix for the output file', default=False)
parser.add_argument('-x', help='Quantity to plot on the x-axis (default x)', default='x')
parser.add_argument('-y', help='Quantity to plot on the y-axis (default y)', default='y')
parser.add_argument('-z', help='Quantity to plot on the z-axis (implemented options: Charge, VD; default Charge)',
                    default='Charge')
parser.add_argument('-npx', help='Binning size of x-axis (default length of X value array)')
parser.add_argument('-npy', help='Binning size of y-axis (default length of Y value array)')
parser.add_argument('-c', help='Condition for the plot', default='')
parser.add_argument('-tcoll',
                    help='ONLY FOR CHARGE COLLECTION: Time in ns for which the charge should be collected (default is 10ns)',
                    type=float,
                    default=10)
parser.add_argument('-tleft',
                    help='ONLY FOR CHARGE COLLECTION AND DRIFT VELOCITY: Choose between tleft and atleft (default tleft)',
                    default='tleft')
parser.add_argument('-type', help='n or p type DUT (default is p type)', default="p")
parser.add_argument('-norm', help='norm by reference laser power, only available for Charge and VD (default is False)',
                    type=str, default=False)
parser.add_argument('-z_t_off', help='Offeset for the Z axis title (default 1.8)',
                    type=float, default=1.8)
parser.add_argument('-rr', help='Real aspect ratio of the x and y axises (default False)', default=False)
parser.add_argument('-inSi',
                    help='Multiply z with a hard coded factor that converts it from "in air" to "in silicon" (default is False)',
                    default=False)
parser.add_argument('-contour', help='Draw a contour line around 0 (default is False)', default=False)


def main():
	"""
	Create the folder plots, if it does not exist.
	"""
	if not os.path.isdir("plots"):
		os.mkdir("plots")

	"""
	Get all arguments from the parser.
	"""
	args = parser.parse_args()
	filename = args.filename
	measurement_name = filename.split(".")[0]
	x_axis = args.x
	y_axis = args.y
	z_axis = args.z
	inSi = utils.str2bool(args.inSi)
	silicon_factor = 3.754
	if inSi:
		if x_axis == "z":
			x_axis = f"{silicon_factor}*{x_axis}"
		if y_axis == "z":
			y_axis = f"{silicon_factor}*{y_axis}"
		if z_axis == "z":
			z_axis = f"{silicon_factor}*{z_axis}"
	z_axis_title_offset = args.z_t_off
	real_aspect_ratio = utils.str2bool(args.rr)
	contour = utils.str2bool(args.contour)
	npx = args.npx
	npy = args.npy
	dut_type = args.type
	norm_by_LPower = utils.str2bool(args.norm)
	condition = args.c
	if not condition == "":
		condition = " && " + condition
	output_appendix = args.o
	voltages = args.v
	exclude_voltages = args.ev
	tcoll = args.tcoll
	tleft = args.tleft
	if dut_type == "p":
		sign = '-'
	else:
		sign = ''
	if norm_by_LPower:
		LPower = 'LPower'
	else:
		LPower = '1'

	"""
	Convert voltages and tcoll to integer, because this looks better in the plot legend.
	If no voltages are specified the bias voltage is readout from the file.
	"""
	if int(tcoll) == tcoll:
		tcoll = int(tcoll)
	if voltages == None:
		voltages = utils.GetBranchAsArray(filename, "ch0", ["Vbias"])[0]
		voltages = utils.GetRoundedValues(voltages, round_to=0)
	else:
		voltages = np.array(voltages).astype(int)
		voltages = np.linspace(voltages[0], voltages[1], voltages[2])
	if not exclude_voltages == None:
		for exclude_voltage in exclude_voltages:
			voltages.remove(int(exclude_voltage))
	"""
	Read in root file from filename and create the ch0 tree.
	"""
	f = rt.TFile(filename)
	tree = f.Get('ch0')

	"""
	Create a TCanvas with size 1000px times 600px, and a TMultigraph which is later filled with the TGraphs.
	The title of the multigraph is set to the measurements name.
	"""

	c1 = rt.TCanvas("c1", "c1", 1000, 600)
	rt.gStyle.SetPalette(87) #  kLightTemperature
	rt.gStyle.SetCanvasBorderMode(0)
	rt.gStyle.SetCanvasBorderSize(0)
	rt.gStyle.SetFrameBorderMode(0)
	rt.gPad.SetRightMargin(0.18)
	rt.gPad.SetLeftMargin(0.12)
	rt.gPad.SetTopMargin(0.1)

	"""
	Create TGraphs for all voltages with the options defined by the parsed arguments.
	"""
	for idx, Vbias in enumerate(voltages):
		if "Vbias" in [x_axis, y_axis, z_axis]:
			Vbias = ""
			condition = args.c
		else:
			Vbias = f"Vbias == {int(Vbias)}"
		if z_axis == 'Charge':
			tree.Draw(
				f"{x_axis}:{y_axis}:{sign}Sum$((volt-BlineMean) * (time-{tleft}>0 && time-{tleft} < {tcoll})) / {LPower}",
				f"{Vbias}{condition}", "l")
		elif z_axis == 'VD':
			tcoll = 0.6
			tree.Draw(
				f"{x_axis}:{y_axis}:{sign}Sum$((volt-BlineMean) * (time-{tleft}>0 && time-{tleft} < {tcoll})) / {LPower}",
				f"{Vbias}{condition}", "l")
		else:
			tree.Draw(f"{x_axis}:{y_axis}:{z_axis} / {LPower}",
			          f"{Vbias}{condition}", "l")
		g2d = rt.TGraph2D(tree.GetSelectedRows(), tree.GetV1(), tree.GetV2(), tree.GetV3())

		"""
		Set the x-, y- ,and z-axis labels, and configure the plot's grid, legend, color scheme,
		and plot binning (npx, npy).
		"""
		if z_axis == "Charge":
			if norm_by_LPower:
				z_axis_title = f"Charge / LPower ({tcoll}ns) [a.u.]"
			else:
				z_axis_title = f"Charge ({tcoll}ns) [a.u.]"
		elif z_axis == "VD":
			if norm_by_LPower:
				z_axis_title = f"Drift velocity / LPower [a.u.]"
			else:
				z_axis_title = f"Drift velocity [a.u.]"
		elif z_axis in ["x", "y", "z", f"{silicon_factor}*z"]:
			if "*" in z_axis:
				z_title = z_axis.split("*")[1]
				z_axis_title = f"{z_title}" + "_{Si} [mm]"
			else:
				z_axis_title = f"Stage {z_axis} [mm]"
		elif "tcoll" in z_axis:
			z_axis_title = f"{z_axis} [ns]"
		else:
			if norm_by_LPower:
				z_axis_title = f"{z_axis} / LPower [a.u.]"
			else:
				z_axis_title = f"{z_axis} [a.u.]"

		if x_axis == "Charge":
			x_axis_title = f"Charge ({tcoll}ns) [a.u.]"
		elif x_axis == "VD":
			x_axis_title = f"Drift velocity [a.u.]"
		elif x_axis in ["x", "y", "z", f"{silicon_factor}*z"]:
			if "*" in x_axis:
				x_title = x_axis.split("*")[1]
				x_axis_title = f"{x_title}" + "_{Si} [mm]"
			else:
				x_axis_title = f"Stage {x_axis} [mm]"
		elif x_axis == "Vbias":
			x_axis_title = f"{x_axis} [V]"
		else:
			x_axis_title = f"{x_axis} [a.u.]"

		if y_axis == "Charge":
			y_axis_title = f"Charge ({tcoll}ns) [a.u.]"
		elif y_axis == "VD":
			y_axis_title = f"Drift velocity [a.u.]"
		elif y_axis in ["x", "y", "z", f"{silicon_factor}*z"]:
			if "*" in y_axis:
				y_title = y_axis.split("*")[1]
				y_axis_title = f"{y_title}" + "_{Si} [mm]"
			else:
				y_axis_title = f"Stage {y_axis} [mm]"
		elif y_axis == "Vbias":
			y_axis_title = f"{y_axis} [V]"
		else:
			y_axis_title = f"{y_axis} [a.u.]"

		try:
			Vbias = Vbias.split("== ")[1]
		except:
			pass

		if "Vbias" in [x_axis, y_axis, z_axis]:
			g2d.SetTitle(f"{measurement_name}; {x_axis_title}; {y_axis_title}; {z_axis_title}")
		else:
			g2d.SetTitle(f"{measurement_name}: Vbias = {Vbias} V; {x_axis_title}; {y_axis_title}; {z_axis_title}")

		try:
			if npx == None:
				for i in x_axis:
					if i in ["x", "y", "z"]:
						npx = len(utils.GetQuantityInputVector(filename, "ch0", i))
			else:
				npx = int(npx)
			if npy == None:
				for i in y_axis:
					if i in ["x", "y", "z"]:
						npy = len(utils.GetQuantityInputVector(filename, "ch0", i))
			else:
				npy = int(npy)

			g2d.SetNpx(npx)
			g2d.SetNpy(npy)
		except:
			sys.exit("\nBinning in x and y needs to be specified using -npx Value and -npy VALUE\nSee trics_2D --help for more information.")

		rt.gPad.SetGrid(1)
		if contour:
			g2d.GetHistogram().DrawCopy("colz")
			g2d.GetHistogram().SetContour(2)
			g2d.GetHistogram().SetContourLevel(1, 0)
			g2d.GetHistogram().Draw("cont3 same")
			g2d.GetHistogram().SetLineColor(1)
			g2d.GetHistogram().SetLineWidth(2)
		else:
			g2d.Draw("colz")
		rt.gPad.Update()
		if real_aspect_ratio:
			c1.SetRealAspectRatio()
		g2d.GetZaxis().SetTitleOffset(z_axis_title_offset)

		"""
		Save the plots as a png and a root file.
		The root file can later be analized with 'root -l  {measurement_name}_vDrift_z_{output_appendix}.root'.
		To open the TCanvas again execute 'c1->Draw()'.
		(Make 2D plot 3D -> open canvas and change the drawing option to lego2)
		"""

		output_name = utils.GetCleanedStr(f"{measurement_name}_{x_axis}_{y_axis}_{z_axis}_{Vbias}V")
		print(output_name)
		# if f"{silicon_factor}" in output_name:
		# 	splitted_output_name = output_name.split(f"{silicon_factor}*z")
		# 	output_name = splitted_output_name[0] + "zSi" + splitted_output_name[1]
		if output_appendix:
			c1.SaveAs(f"plots/{output_name}_{output_appendix}.png")
			c1.SaveAs(f"plots/{output_name}_{output_appendix}.root")
		else:
			c1.SaveAs(f"plots/{output_name}.png")
			c1.SaveAs(f"plots/{output_name}.root")
		if Vbias == "":
			break
	c1.Close()

if __name__ == '__main__':
	main()
