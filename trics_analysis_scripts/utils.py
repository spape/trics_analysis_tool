from ROOT import RDataFrame
import numpy as np
from collections import Counter


def linear(x, m, b):
	"""
	Linear function that can be used for fits.

	Parameters
	----------
	x : array or intfloat
		x value or x array for the linear function.
	m : float
		Slope of the linear function.
	b : float
		Constant offset of the linear function.

	Returns
	-------
	m * x + b : array or float
		Result (y value or y array) of the linear function.
	"""
	return m * x + b


def GetDictOfArrays(filename, treename, list_branches):
	"""
	This function returns input branch names from a given root file as a dict.

	Parameters
	----------
	filename : str
		Name of the root file.
	treename : str
		Name of the TTree.
	list_branches : list[str]
		List with names of branches, that will be transformed to a dict.

	Returns
	-------
	dict
		Dictionary with the requested branches as keys.
	"""
	df = RDataFrame(treename, filename)
	return df.AsNumpy(list_branches)


def GetBranchAsArray(filename, treename, list_branches):
	"""
	This function returns input branch names from a given root file as a list of arrays.

	Parameters
	----------
	filename : str
		Name of the root file.
	treename : str
		Name of the TTree.
	list_branches : list[str]
		List with names of branches, that will be transformed to a dict.

	Returns
	-------
	list[arrays]
		List of arrays in the order of the given branch list list_branches.
	"""
	dict_branches = GetDictOfArrays(filename, treename, list_branches)
	list_arrays = []
	for key in list_branches:
		list_arrays.append(dict_branches[key])
	return list_arrays


def GetVectorAsArray(Vector):
	"""
	This function converts ROOT vectors to python arrays.

	Parameters
	----------
	Vector : ROOT Vector
		Input ROOT vector.

	Returns
	-------
	Array : list[arrays]
		Converted vector.
	"""
	Array = []
	for v in Vector:
		Array.append(list(v.AsVector()))
	return Array

def str2bool(v):
	"""
	Converts a string that contains a boolean value to an actual boolean.
	If the input string does not contain a boolen value, an argument type error is raised.

	Parameters
	----------
	v : str
		String that is evaluated.

	Returns
	-------
	bool
		Boolean that corresponds to the input string v.
	"""
	if isinstance(v, bool):
		return v
	if v.lower() in ('yes', 'true', 't', 'y', '1'):
		return True
	elif v.lower() in ('no', 'false', 'f', 'n', '0'):
		return False
	else:
		raise argparse.ArgumentTypeError('Boolean value expected.')


def GetTGraphValues(TGraph):
	"""
	Function that returns the x and y values of a TGraph.

	Parameters
	----------
	TGraph : ROOT.TGraph
		Input TGraph.

	Returns
	-------
	list(TGraph.GetX()) : list[float]
		List of x values of the input TGraph.
	list(TGraph.GetY()) : list[float]
		List of y values of the input TGraph.
	"""
	return list(TGraph.GetX()), list(TGraph.GetY())

def GetTGraphDerivative(TGraph):
	"""
	Returns the x and y values of the derivative of a TGraph.

	Parameters
	----------
	TGraph : ROOT.TGraph
		Input TGraph.

	Returns
	-------
	X : list[float]
		List of x values of the input TGraph.
	derivative : list[float]
		Derivation of the y values of the input TGraph.
	"""
	X = list(TGraph.GetX())
	Y = list(TGraph.GetY())
	derivative = np.gradient(Y)
	return X, derivative

def GetMinMax(TGraph):
	"""
	Function that returns the Max and Min and the corresponding indixes of a TGraph.

	Parameters
	----------
	TGraph : ROOT.TGraph
		Input TGraph.

	Returns
	-------
	minimum : float
		Minimum of the input TGraph.
	idx_min : int
		To the minimum corresponding array index of the input TGraph.
	maximum : float
		Minimum of the input TGraph.
	idx_max : int
		To the maximum corresponding array index of the input TGraph.
	"""
	YValues = list(TGraph.GetY())
	minimum = min(YValues)
	idx_min = YValues.index(minimum)
	maximum = max(YValues)
	idx_max = YValues.index(maximum)
	return minimum, idx_min, maximum, idx_max


def GetFirstHalfTPAMax(TGraph):
	"""
	Function that returns the X value of the first FWHM and the corresponding indixe of a TGraph.

	Parameters
	----------
	TGraph : ROOT.TGraph
		Input TGraph.

	Returns
	-------
	XValue : float
		XValue for the corresponding YValue.
	idx_YValue : int
		Array index in the input TGraph, where the input YValue is located.
	"""
	minimum, idx_min, maximum, idx_max = GetMinMax(TGraph)
	if abs(maximum) > abs(minimum):
		YValue = (maximum - minimum) / 2 + minimum
		cut_idx = idx_max
	else:
		YValue = (minimum - maximum) / 2 + maximum
		cut_idx = idx_min
	YValues = list(TGraph.GetY())[:cut_idx]
	XValues = list(TGraph.GetX())[:cut_idx]
	differences_YValue = list(abs(np.array(YValues) - YValue))
	idx_YValue = differences_YValue.index(min(differences_YValue))

	XValue = list(XValues)[idx_YValue]
	return XValue, idx_YValue


def GetSecondHalfTPAMax(TGraph):
	"""
	Function that returns the X value of the second FWHM and the corresponding indixe of a TGraph.

	Parameters
	----------
	TGraph : ROOT.TGraph
		Input TGraph.

	Returns
	-------
	XValue : float
		XValue for the corresponding YValue.
	idx_YValue : int
		Array index in the input TGraph, where the input YValue is located.
	"""
	minimum, idx_min, maximum, idx_max = GetMinMax(TGraph)
	if abs(maximum) > abs(minimum):
		YValue = (maximum - minimum) / 2 + minimum
		cut_idx = idx_max
	else:
		YValue = (minimum - maximum) / 2 + maximum
		cut_idx = idx_min
	YValues = list(TGraph.GetY())[cut_idx:]
	XValues = list(TGraph.GetX())[cut_idx:]
	differences_YValue = list(abs(np.array(YValues) - YValue))
	idx_YValue_cutted = differences_YValue.index(min(differences_YValue))
	idx_YValue = idx_YValue_cutted + cut_idx

	XValue = list(XValues)[idx_YValue_cutted]
	return XValue, idx_YValue


def GetRoundedValues(X, round_to=3):
	"""
	Get the set value for an measured value. E.g. when an z scan is done for fix x and y values,
	the x and y set values are not easily accessable. To receive just the set values rounded to
	the 'round_to' digit, this function can be used.

	Parameters
	----------
	X : list
		Input list with measured values.
	round_to : int, optional
		Digit to which the output values will be rounded.

	Returns
	-------
	XSet : list(float)
		XSet value rounded to the 'round_to' digit.
	"""
	if round_to:
		counter = np.array(list(Counter(np.round(X, round_to)).keys())).astype(float)
		XSet = list(counter)
	else:
		counter = np.array(list(Counter(X).keys()))
		XSet = list(counter.astype(int))
	return XSet

def GetQuantityInputVector(filename, treename, quantity):
	"""
	Convert a iterative quantity to the actual value.
	In the root branch the quantities are always included for all events.
	This function helps get the input vector that was used to iterate the quantity.

	Parameters
	----------
	filename : str
		Name of the root file.
	treename : str
		Name of the TTree.
	quantity : str
		Name of the quantity you want to convert (e.g. 'x' or 'y' or 'z').

	Returns
	-------
	converted_quantity : list[float]
		List of the converted quantity.
	"""
	iquantity, quantity = GetBranchAsArray(filename, treename, [f"i{quantity}", f"{quantity}"])
	abs_min_iquantity = abs(min(iquantity))
	iquantity = [i + abs_min_iquantity for i in iquantity]
	counter = np.array(list(Counter(iquantity).keys()))
	converted_quantity = []
	for idx in counter:
		converted_quantity.append(np.round(quantity[iquantity.index(idx)], 4))
	return converted_quantity


def GetCleanedStr(string, unwanted_characters=["(", ")", ":", "/", "*"]):
	"""
	Clean an input string from unwanted characters.

	Parameters
	----------
	string : str
		Input string.
	unwanted_characters : list[str]
		List of unwanted characters. The default unwanted characters are (,), and :.

	Returns
	-------
	string: str
		Cleaned input string.
	"""
	for uc in unwanted_characters:
		string = string.replace(uc, "")
	return string


def SetMatplotlibParams(default_colormap='rainbow'):
    from matplotlib import rcParams
    import matplotlib as plt

    rcParams['font.family'] = 'DejaVu Sans'
    rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'

    rcParams['font.size'] = 10
    rcParams['axes.formatter.use_mathtext'] = True
    rcParams['legend.fontsize'] = 12
    rcParams['savefig.dpi'] = 300
    rcParams['savefig.bbox'] = 'tight'
    rcParams['savefig.pad_inches'] = 0.1
    rcParams['image.cmap'] = default_colormap


def ScaleZ(w0, z):
	"""
	Scale z values. This is needed, when the beam is inside silicon.

	Parameters
	----------
	w0 : float
		minimal beam waist w0 in µm.
	z : float or np.array
		Input z values in µm.

	Returns
	-------
	flaot
		Scaled z values.
	"""
	wavelength = 1.55 #  µm
	n = 3.4757 #  From paper DOI 10.1109
	z0 = np.pi * w0**2 * n / wavelength
	return z * np.sqrt(z0 * np.pi * n**3 / (z0 * np.pi * n - wavelength * n**2  + wavelength))
