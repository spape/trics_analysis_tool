"""
Script to fit on Knife Edge data (xz Scan) of unirradiated devices with the charge carrier density function developed in
DOI 10.1109.
Build to work with data from a TPA-TCT measurement, taken with the TCT- setup at CERN and
analized with the TRICS framework.
Developer: Sebastian Pape
E-Mail: sebastian.pape@cern.ch


This script is executable from the terminal with 'trics_KnifeEdge'.
Required arguments for the parser:
    filename: name of the root file
    -x: range of fit -x start -x stop
    -iz: iz position for knife edge fit
Optional arguments for the parser:
    -v: voltages as -v start -v stop -v steps
    -o: appendix for the output file
    -c: Condition for the plot
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)
    -type: n or p type DUT (default is p type)
    -plot: Create a plot for every fit of Q(x,z) (default False)
"""

import argparse
import os
import sys
import ROOT as rt
import numpy as np
import matplotlib.pyplot as plt
from IV_CV_Plotter.IV_CV_Plot import mean
from scipy.optimize import curve_fit
from . import utils

"""
Argument definitons for the parser.
"""
parser = argparse.ArgumentParser()
parser.add_argument('filename', help='name of the root file')
parser.add_argument('-x', action='append', type=float, help='range of fit -x start -x stop', required=True)
parser.add_argument('-iz', action='append', type=int, help='iz position for knife edge fit', required=True)
parser.add_argument('-v', action='append', help='voltages as -v start -v stop -v steps')
parser.add_argument('-o', help='appendix for the output file', default=False)
parser.add_argument('-c', help='Condition for the plot', default='')
parser.add_argument('-tcoll', help='Time in ns for which the charge should be collected (default is 10ns)', type=float,
                    default=10)
parser.add_argument('-type', help='n or p type DUT (default is p type)', default="p")
parser.add_argument('-plot', help='Create a plot for every fit of Q(x,z) (default False)', type=str, default=False)

"""
Definition of the fitting function.
"""
def charge_carrier_density_in_x_rightedge(start, stop):
	return rt.TF1("charge_carrier_density_tpa", f"[0] / [1]**2 * (1 + erf(-2*(x - [2])/[1]))", start, stop) #  [0] amp, [1] w(z), [2] z0


def charge_carrier_density_in_x_leftedge(start, stop):
	return rt.TF1("charge_carrier_density_tpa", f"[0] / [1]**2 * (1 + erf(2*(x - [2])/[1]))", start, stop) #  [0] amp, [1] w(z), [2] z0


def func_w_z(z, z_0, w_0, z_off):
	return w_0 * np.sqrt(1 + ((z - z_off) / z_0)**2)


def main():
	"""
	Get all arguments from the parser.
	"""
	silicon_factor = 3.754
	args = parser.parse_args()
	filename = args.filename
	measurement_name = filename.split(".")[0]
	"""
	Create the folder plots, if it does not exist.
	"""
	save_dir = f"plots/KnifeEdge_{measurement_name}"
	if not os.path.isdir(save_dir):
		os.mkdir(save_dir)
	output_appendix = args.o
	tcoll = args.tcoll
	dut_type = args.type
	voltages = args.v
	iz_min_max = np.array(args.iz).astype(int)
	create_plot = utils.str2bool(args.plot)
	condition = args.c
	if not condition == "":
		condition = " && " + condition
	if dut_type == "p":
		sign = '-'
	else:
		sign = ''

	x_range = np.array(args.x).astype(float)
	z = utils.GetQuantityInputVector(filename, "ch0", "z")

	"""
	Convert the voltages to integer, because this looks better in the plot legend.
	If no voltages are specified the bias voltage is readout from the file.
	"""
	if voltages == None:
		voltages = utils.GetBranchAsArray(filename, "ch0", ["Vbias"])[0]
		voltages = utils.GetRoundedValues(voltages, round_to=0)
	else:
		for idx, v in enumerate(voltages):
			voltages[idx] = int(v)
		np.linspace(voltages[0], voltages[1], voltages[2])
	iz_array_complete = utils.GetBranchAsArray(filename, "ch0", ["iz"])[0]
	iz_array_complete = utils.GetRoundedValues(iz_array_complete, round_to=0)
	cut_iz_min = iz_array_complete.index(iz_min_max[0])
	cut_iz_max = iz_array_complete.index(iz_min_max[1])
	iz_array = iz_array_complete[cut_iz_min:cut_iz_max + 1]


	f = rt.TFile(filename)
	tree = f.Get('ch0')

	"""
	Create a TCanvas that will be filled with the graphs.
	"""
	c1 = rt.TCanvas("c1", "c1")

	"""
	Create TGraphs for iteration over voltages with the options defined by the parsed arguments.
	The TGraphs data is dumped into a TH1D, which is further used to peform the fitting.
	The plots are further customized with axis labels and a legend.
	The final results get saved in a png and a root file.
	"""
	c1 = rt.TCanvas("c1", "c1")
	w_z = []
	w_z_err = []
	for iz in iz_array:
		for idx, Vbias in enumerate(voltages):
			tree.Draw(f"{sign}Sum$((volt-BlineMean) * (time-tleft>0 && time-tleft < {tcoll})):x",
			          f"Vbias=={Vbias} && TMath::Abs(iz - {iz}) < 1e-1{condition}", "goff")

			"""
			Set parameters for fit function.
			"""
			nb = tree.GetSelectedRows()
			y = tree.GetV1()
			x = tree.GetV2()
			graph = rt.TGraph(nb, x, y)
			minimum, _, maximum, _ = utils.GetMinMax(graph)
			if abs(maximum) > abs(minimum):
				estimate_const = maximum / 1e4
			else:
				estimate_const = minimum / 1e4
			x_array, y_array = utils.GetTGraphValues(graph)
			try:
				#  if the last value of Q(x) is higher than the first one, the second half maximum is needed as an offset estimate
				y_sorted = np.array(y_array)[np.argsort(x_array)]
				if y_sorted[0] < y_sorted[-1]:
					"""
					Initialize fit function when the active volume is on the right
					"""
					fit_func = charge_carrier_density_in_x_leftedge(x_range[0], x_range[1])
				else:
					fit_func = charge_carrier_density_in_x_rightedge(x_range[0], x_range[1])
				"""
				Get an estimate for the offset from x = 0.
				This can not be checked in the if condition above, because the functions to get the half maxima
				do not check if the x array is sorted, or reversed --> lesads to problems, therefore the if
				condition below is needed.
				"""
				if y_array[0] < y_array[-1]:
					estimate_offset, _ = utils.GetFirstHalfTPAMax(graph)
				else:
					estimate_offset, _ = utils.GetSecondHalfTPAMax(graph)
			except:
				#  if calculation of the Half maximum calculation fails a random value (here 0.1) is used.
				estimate_offset = 0.3

			estimate_w = 0.1
			print(estimate_w, estimate_const, estimate_offset)
			fit_func.SetParameters(estimate_const, estimate_w, estimate_offset)
			x_axis_input_values = utils.GetQuantityInputVector(filename, "ch0", "x")

			bin_size = abs((x_axis_input_values[1] - x_axis_input_values[0]) / 2)
			h1 = rt.TH1D(f"iz:{iz}, {int(Vbias)} V", measurement_name + "   Fit_func:" + fit_func.GetTitle(), nb,
			             min(x_axis_input_values) - bin_size, max(x_axis_input_values) + bin_size)

			for x_value, y_value in zip(x_axis_input_values, y_array):
				h1.Fill(x_value, y_value)
			h1.Draw("hist")
			h1.Fit(fit_func.GetName(), "WW R")
			fit = h1.GetFunction(fit_func.GetName())
			params = []
			for i in range(3):
				try:
					params.append(fit.GetParameter(i))
				except:
					break

			w_z.append(fit.GetParameter(1) * 1e3)
			w_z_err.append(fit.GetParError(1) * 1e3)

			if create_plot:
				h1.GetXaxis().SetTitle(r"Stage x [mm]")
				try:
					h1.GetYaxis().SetTitle(f"Charge ({int(tcoll)}ns) [a.u.]")
				except:
					h1.GetYaxis().SetTitle(f"Charge ({tcoll}ns) [a.u.]")
				fit_func.SetParameters(*params)
				fit_func.Draw("same")
				rt.gStyle.SetOptFit(1)
				rt.gPad.SetGrid(1)
				rt.gPad.Update()

				if output_appendix:
					c1.SaveAs(f"{save_dir}/{measurement_name}_x_fit_iz{iz}_{int(Vbias)}V_{output_appendix}.png")
					c1.SaveAs(f"{save_dir}/{measurement_name}_x_fit_iz{iz}_{int(Vbias)}V_{output_appendix}.root")
				else:
					c1.SaveAs(f"{save_dir}/{measurement_name}_x_fit_iz{iz}_{int(Vbias)}V.png")
					c1.SaveAs(f"{save_dir}/{measurement_name}_x_fit_iz{iz}_{int(Vbias)}V.root")

	c1.Close()
	utils.SetMatplotlibParams()
	plt.clf()
	# z = np.array(z[min(iz_array) - 1:max(iz_array)]) * silicon_factor
	z = np.array(z[min(iz_array) - 1:max(iz_array)])
	z_plot = np.linspace(min(z), max(z), 10000)
	# params_w, covariance = curve_fit(func_w_z, z, w_z, sigma=w_z_err, p0=[0.01, 0.01, np.mean(z)], bounds=([0, 0, -np.inf], [1, np.inf, np.inf]))
	params_w, covariance = curve_fit(func_w_z, z, w_z, p0=[0.01, 0.01, np.mean(z)], bounds=([0, 0, -np.inf], [1, np.inf, np.inf]))
	fig = plt.figure()
	ax = plt.axes()
	# plt.xlabel("$z_{\mathrm{Si}}$ [mm]")
	plt.xlabel("Stage $z$ [mm]")
	plt.ylabel("$w$ [µm]")

	w_0 = np.round(params_w[1], 3)
	z_0 = np.round(params_w[0] * 1e3, 3)
	w_0_err = np.round(np.sqrt(np.diag(covariance))[1], 3)
	z_0_err = np.round(np.sqrt(np.diag(covariance))[0] * 1e3, 3)

	# plt.errorbar(z, w_z, w_z_err, fmt="k.", label="Data")
	plt.plot(z, w_z, ".",  color="k", label="Data")
	plt.plot(z_plot, func_w_z(z_plot, *params_w), color="r", label="Fit")
	# plt.title(measurement_name + f"\n$w_0$ = ({w_0}$\,\pm\,${w_0_err})$\,$µm" + r", $z_{0, \mathrm{Si}}$ = " + f"({z_0}$\,\pm\,${z_0_err})$\,$µm")
	# plt.title(measurement_name + f"\n$w_0$ = {w_0}$\,$µm" + r", $z_{0, \mathrm{Si}}$ = " + f"{z_0}$\,$µm")
	plt.title(measurement_name + f"\n$w_0$ = {w_0}$\,$µm" + r", $z_{0, \mathrm{Si}}$ = " + f"{np.round(utils.ScaleZ(w_0, z_0), 2)}$\,$µm")
	plt.legend()
	plt.grid()
	plt.tight_layout()
	plt.savefig(f"{save_dir}/{measurement_name}_w_z_plot.png")
	#  plot w(z) with iz on the x axis
	if create_plot:
		plt.clf()
		iz_plot = range(iz_min_max[0], iz_min_max[1] + 1)
		fig = plt.figure()
		ax = plt.axes()
		plt.xlabel("$iz$")
		plt.ylabel("$w$ [µm]")

		# plt.errorbar(iz_plot, w_z, w_z_err, fmt="k.", label="Data")
		plt.plot(iz_plot, w_z, ".", color="k", label="Data")
		plt.plot(np.linspace(iz_min_max[0], iz_min_max[1], len(z_plot)), func_w_z(z_plot, *params_w), color="r", label="Fit")
		# plt.title(measurement_name + f"\n$w_0$ = {w_0}$\,$µm" + r", $z_{0, \mathrm{Si}}$ = " + f"{z_0}$\,$µm")
		# plt.title(measurement_name + f"\n$w_0$ = ({w_0}$\,\pm\,${w_0_err})$\,$µm" + r", $z_{0, \mathrm{Si}}$ = " + f"({z_0}$\,\pm\,${z_0_err})$\,$µm")
		plt.title(measurement_name + f"\n$w_0$ = ({w_0}$\,\pm\,${w_0_err})$\,$µm" + r", $z_{0}$ = " + f"{z_0}$\,$µm")
		plt.legend()
		plt.grid()
		plt.tight_layout()
		plt.savefig(f"{save_dir}/{measurement_name}_w_z_plot_iz.png")

if __name__ == '__main__':
	main()
