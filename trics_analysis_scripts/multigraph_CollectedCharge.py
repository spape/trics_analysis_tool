"""
ch0->Draw(f"-Sum$((volt-BlineMean) * (time-tleft>0 && time-tleft < {tcoll})):z", f" Vbias=={Vbias}  {condition}", "l")
"""
"""
Script to create charge colletion plots from a TPA-TCT measurement, taken with the TCT- setup at CERN and
analized with the TRICS framework.
Developer: Sebastian Pape
E-Mail: sebastian.pape@cern.ch


This script is executable from the terminal with 'trics_CollectedCharge'.
Required arguments for the parser:
    filename: name of the root file
Optional arguments for the parser:
    -v: voltages as -v start -v stop -v steps
    -o: appendix for the output file
    -x: Quantity to plot on the x-axis (default z)
    -y: Quantity to plot on the y-axis (default Charge)
    -c: Condition for the plot
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)
    -tleft: Choose between tleft and atleft (default tleft)
    -type: n or p type DUT (default is p type)
    -norm: norm by reference laser power, only available for Charge and VD (default is False)
    -inSi: Multiply x-axis quantity with a hard coded factor that converts it from "in air" to "in silicon" (default is False)
"""

import argparse
import os
import ROOT as rt
import numpy as np
from . import utils

"""
Argument definitons for the parser. 
"""
parser = argparse.ArgumentParser()
parser.add_argument('filename', help='name of the root file')
parser.add_argument('-v', action='append', help='voltages as -v start -v stop -v steps')
parser.add_argument('-o', help='appendix for the output file', default=False)
parser.add_argument('-x', help='Quantity to plot on the x-axis (default z)', default='z')
parser.add_argument('-y', help='Quantity to plot on the y-axis (default Charge)', default='Charge')
parser.add_argument('-c', help='Condition for the plot', default='')
parser.add_argument('-tcoll', help='Time in ns for which the charge should be collected (default is 10ns)', type=float,
                    default=10)
parser.add_argument('-tleft', help='Choose between tleft and atleft (default tleft)', default='tleft')
parser.add_argument('-type', help='n or p type DUT (default is p type)', default="p")
parser.add_argument('-norm', help='norm by reference laser power, only available for Charge and VD (default is False)',
                    type=str, default=False)
parser.add_argument('-inSi',
                    help='Multiply x-axis quantity with a hard coded factor that converts it from "in air" to "in silicon" (default is False)',
                    default=False)


def main():
	"""
	Create the folder plots, if it does not exist.
	"""
	if not os.path.isdir("plots"):
		os.mkdir("plots")

	"""
	Get all arguments from the parser. 
	"""
	args = parser.parse_args()
	filename = args.filename
	measurement_name = filename.split(".")[0]
	x_axis = args.x
	y_axis = args.y
	inSi = utils.str2bool(args.inSi)
	silicon_factor = 3.754
	if inSi:
		x_axis = f"{silicon_factor}*{x_axis}"
	condition = args.c
	if not condition == "":
		condition = " && " + condition
	dut_type = args.type
	norm_by_LPower = utils.str2bool(args.norm)
	output_appendix = args.o
	tcoll = args.tcoll
	voltages = args.v

	tleft = args.tleft
	if dut_type == "p":
		sign = '-'
	else:
		sign = ''
	if norm_by_LPower:
		LPower = 'LPower'
	else:
		LPower = '1'

	"""
	Convert voltages and tcoll to integer, because this looks better in the plot legend.
	If no voltages are specified the bias voltage is readout from the file.
	"""
	if int(tcoll) == tcoll:
		tcoll = int(tcoll)
	if voltages == None:
		voltages = utils.GetBranchAsArray(filename, "ch0", ["Vbias"])[0]
		voltages = utils.GetRoundedValues(voltages, round_to=0)
	else:
		voltages = np.array(voltages).astype(int)
		voltages = np.linspace(voltages[0], voltages[1], voltages[2])

	"""
	Read in root file from filename and create the ch0 tree. 
	"""
	f = rt.TFile(filename)
	tree = f.Get('ch0')

	"""
	Create a TCanvas with size 1000px times 600px, and a TMultigraph which is later filled with the TGraphs.
	The title of the multigraph is set to the measurements name.
	"""
	c1 = rt.TCanvas("c1", "c1", 1000, 600)
	mg = rt.TMultiGraph()
	mg.SetTitle(measurement_name)

	"""
	Create TGraphs for all voltages with the options defined by the parsed arguments. 
	"""
	lstyle = 1
	lcolor = 1
	for idx, Vbias in enumerate(voltages):
		Vbias = int(Vbias)
		if y_axis == 'Charge':
			tree.Draw(
				f"{sign}Sum$((volt-BlineMean) * (time-{tleft}>0 && time-{tleft} < {tcoll})) / {LPower}:{x_axis}",
				f"Vbias=={Vbias}{condition}", "l")
		else:
			tree.Draw(f"{y_axis}:{x_axis}", f"Vbias=={Vbias}{condition}", "l")
		graph = rt.TGraph(tree.GetSelectedRows(), tree.GetV2(), tree.GetV1())
		graph.SetName(f"{Vbias} V")
		graph.SetTitle(f"{Vbias} V")
		# if lstyle > 3:
		# 	lstyle = 1
		# 	lcolor += 1
		# 	if lstyle == 3:
		# 		lstyle == 6
		# if lcolor == 5:
		# 	lcolor = 6
		# if lcolor == 9:
		# 	lcolor = 1
		# graph.SetMarkerStyle(7)
		# graph.SetMarkerColor(lcolor)
		# graph.SetLineStyle(lstyle)
		# graph.SetLineColor(lcolor)
		# lstyle += 1
		graph.SetLineWidth(3)
		mg.Add(graph)

	"""
	Set the x- and y-axis labels, and configure the plot's grid, legend, and color scheme. 
	"""
	if y_axis == "Charge":
		if norm_by_LPower:
			mg.GetYaxis().SetTitle(f"Charge / LPower ({tcoll}ns) [a.u.]")
		else:
			mg.GetYaxis().SetTitle(f"Charge ({tcoll}ns) [a.u.]")
	elif y_axis in ["x", "y", "z"]:
		mg.GetYaxis().SetTitle(f"Stage {y_axis} [mm]")
	else:
		mg.GetYaxis().SetTitle(f"{y_axis} [a.u.]")

	if x_axis == "Charge":
		if norm_by_LPower:
			mg.GetXaxis().SetTitle(f"Charge / LPower ({tcoll}ns) [a.u.]")
		else:
			mg.GetXaxis().SetTitle(f"Charge ({tcoll}ns) [a.u.]")
	elif x_axis in ["x", "y", "z", f"{silicon_factor}*x", f"{silicon_factor}*y", f"{silicon_factor}*z"]:
		if inSi:
			y_tile = x_axis.split("*")[1]
			mg.GetXaxis().SetTitle(f"{y_tile}" + "_{Si} [mm]")
		else:
			mg.GetXaxis().SetTitle(f"Stage {x_axis} [mm]")
	else:
		mg.GetXaxis().SetTitle(f"{x_axis} [a.u.]")
	# mg.Draw("awmp awlp pmc plc")
	rt.gPad.SetGrid(1)
	rt.gStyle.SetPalette(87) #  kLightTemps
	mg.Draw("al pmc plc")
	c1.BuildLegend(0.902, 0.1, 0.97, 0.9)
	rt.gPad.Update()

	"""
	Save the plots as a png and a root file.
	The root file can later be analized with 'root -l  {measurement_name}_vDrift_z_{output_appendix}.root'.
	To open the TCanvas again execute 'c1->Draw()'.
	"""
	output_name = utils.GetCleanedStr(f"{measurement_name}_CollectedCharge")
	if output_appendix:
		c1.SaveAs(f"plots/{output_name}_{output_appendix}.png")
		c1.SaveAs(f"plots/{output_name}_{output_appendix}.root")
	else:
		c1.SaveAs(f"plots/{output_name}.png")
		c1.SaveAs(f"plots/{output_name}.root")

	c1.Close()


if __name__ == '__main__':
	main()
