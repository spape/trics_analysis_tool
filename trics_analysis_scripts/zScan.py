"""
Script to fit z-Scans of unirradiated devices with the charge carrier density function developed in DOI 10.1109.
Build to work with data from a TPA-TCT measurement, taken with the TCT- setup at CERN and
analized with the TRICS framework.
Developer: Sebastian Pape
E-Mail: sebastian.pape@cern.ch


This script is executable from the terminal with 'trics_zScan'.
Required arguments for the parser:
    filename: name of the root file
    -na: numerical aperture of the used objective (0.5 or 0.7)
Optional arguments for the parser:
    -z: range of fit -z start -z stop
    -v: voltages as -v start -v stop -v steps
    -o: appendix for the output file
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)
    -irrad: Clarrify if the DUT is irradiated, select True, if irradiated and file is not spa corrected (default False)
    -type: n or p type DUT (default is p type)
    -inSi: Multiply the z values with a hard coded factor that converts it from "in air" to "in silicon" (default is False)
"""

import argparse
import os
import sys
import ROOT as rt
import numpy as np
from . import utils

"""
Argument definitons for the parser.
"""
parser = argparse.ArgumentParser()
parser.add_argument('filename', help='name of the root file')
parser.add_argument('-na', help='numerical aperture of the used objective (0.5 or 0.7)', required=True, type=float)
parser.add_argument('-chain', action='append', help='Chain files to the input file')
parser.add_argument('-z', action='append', type=float, help='range of fit -z start -z stop')
parser.add_argument('-v', action='append', help='voltages as -v start -v stop -v steps')
parser.add_argument('-o', help='appendix for the output file', default=False)
parser.add_argument('-tcoll', help='Time in ns for which the charge should be collected (default is 10ns)', type=float,
                    default=10)
parser.add_argument('-irrad', help='Clarrify if the DUT is irradiated, select True, if irradiated and file is not spa corrected (default False)', default=False)
parser.add_argument('-type', help='n or p type DUT (default is p type)', default="p")
parser.add_argument('-inSi',
                    help='Multiply the z values with a hard coded factor that converts it from "in air" to "in silicon" (default is False)',
                    default=False)

"""
Definition of the fitting function.
"""
def charge_carrier_density_in_z(start, stop):
	return rt.TF1("charge_carrier_density_tpa", f"[0] * (atan(([1] - (x - [2])) / [3]) + atan((x - [2]) / [3]))",
	              start, stop) #  [0]: const, [1]: d, [2]: offset from z=0, [3]: z0

def charge_carrier_density_in_z_irrad(start, stop):
	return rt.TF1("charge_carrier_density_tpa", f"[0] * (atan(([1] - (x - [2])) / [3]) + atan((x - [2]) / [3])) + [4]",
	              start, stop) #  [0]: const, [1]: d, [2]: offset from z=0, [3]: z0, [4]: SPA offset


def main():
	"""
	Get all arguments from the parser.
	"""
	args = parser.parse_args()
	filename = args.filename
	chain = args.chain
	measurement_name = filename.split(".")[0]
	na = args.na
	irrad = utils.str2bool(args.irrad)
	"""
	Create the folder plots, if it does not exist.
	"""
	save_dir = f"plots/zScan_{measurement_name}"
	if not os.path.isdir(save_dir):
		os.mkdir(save_dir)
	output_appendix = args.o
	charge_in_x_ns = args.tcoll
	dut_type = args.type
	voltages = args.v
	z_range = args.z
	if z_range == None:
		print("No z range specified. Using the whole z range.")
		z_array = utils.GetBranchAsArray(filename, "ch0", ["z"])[0]
		z_range = np.array([min(z_array), max(z_array)])
	else:
		z_range = np.array(args.z).astype(float)

	inSi = utils.str2bool(args.inSi)
	# silicon_factor = 3.754
	# if inSi:
	# 	z_range *= silicon_factor
	# 	z_for_plot = f"{silicon_factor}*z"
	# else:
	# 	z_for_plot = "z"
	z_for_plot = "z"

	"""
	Initialize fit function.
	"""
	if irrad:
		fit_func = charge_carrier_density_in_z_irrad(z_range[0], z_range[1])
	else:
		fit_func = charge_carrier_density_in_z(z_range[0], z_range[1])

	f = rt.TFile(filename)
	"""
	If data is stored in multiple root files, a TChain can be created -> This treates the two root files as one.
	"""
	if not chain == None:
		tree = rt.TChain("ch0")
		tree.Add(filename)
		filename = [filename]
		for file in chain:
			tree.Add(file)
			filename.append(file)
	else:
		tree = f.Get('ch0')

	"""
	Convert the voltages to integer, because this looks better in the plot legend.
	If no voltages are specified the bias voltage is readout from the file.
	"""
	if voltages == None:
		voltages = utils.GetBranchAsArray(filename, "ch0", ["Vbias"])[0]
		voltages = utils.GetRoundedValues(voltages, round_to=0)
		sort_indices = np.argsort(np.abs(voltages))
		voltages = np.array(voltages)[sort_indices]
	else:
		voltages = np.array(voltages).astype(int)
		voltages = np.linspace(voltages[0], voltages[1], voltages[2])

	"""
	Create a TCanvas that will be filled with the graphs.
	"""
	c1 = rt.TCanvas("c1", "c1")

	"""
	Create TGraphs for iteration over voltages with the options defined by the parsed arguments.
	The TGraphs data is dumped into a TH1D, which is further used to perform the fitting.
	The plots are former customized with axis labels and a legend.
	The final results get saved in a png and a root file.
	"""
	thickness = []
	thickness_derivative = []
	rayleigh = []
	for idx, Vbias in enumerate(voltages):
		if dut_type == "p":
			tree.Draw(f"-Sum$((volt-BlineMean) * (time-tleft>0 && time-tleft < {charge_in_x_ns})):{z_for_plot}",
			          f"Vbias=={Vbias}", "goff")
		else:
			tree.Draw(f"Sum$((volt-BlineMean) * (time-tleft>0 && time-tleft < {charge_in_x_ns})):{z_for_plot}",
			          f"Vbias=={Vbias}", "goff")

		"""
		Set parameters for fit function.
		"""
		nb = tree.GetSelectedRows()
		y = tree.GetV1()
		x = tree.GetV2()
		graph = rt.TGraph(nb, x, y)
		x, dy = utils.GetTGraphDerivative(graph)
		mask_for_first_half = [x < (z_range[1] + z_range[0]) / 2]
		mask_for_second_half = [x > (z_range[1] + z_range[0]) / 2]
		thickness_derivative.append((x[list(dy).index(min(dy[mask_for_second_half]))] - x[list(dy).index(max(dy[mask_for_first_half]))]) * 1e3)

		minimum, _, maximum, _ = utils.GetMinMax(graph)
		if abs(maximum) > abs(minimum):
			estimate_amp = (maximum - minimum) / 2 + minimum
		else:
			estimate_amp = (minimum - maximum) / 2 + maximum
		estimate_offset, _ = utils.GetFirstHalfTPAMax(graph)
		secondHM, _ = utils.GetSecondHalfTPAMax(graph)
		estimate_d = abs(estimate_offset - secondHM)
		estimate_z0 = 0.003

		if inSi:
			estimate_z0 *= silicon_factor
		if irrad:
			fit_func.SetParameters(estimate_amp, estimate_d, estimate_offset, estimate_z0, minimum)
		else:
			fit_func.SetParameters(estimate_amp, estimate_d, estimate_offset, estimate_z0)

		z_axis_input_values = utils.GetQuantityInputVector(filename, "ch0", "z")
		if inSi:
			z_axis_input_values = silicon_factor * np.array(z_axis_input_values)

		bin_size = abs((z_axis_input_values[1] - z_axis_input_values[0]) / 2)
		h1 = rt.TH1D(f"{int(Vbias)} V", measurement_name + "   Fit_func:" + fit_func.GetTitle(), nb,
		             min(z_axis_input_values) - bin_size, max(z_axis_input_values) + bin_size)
		for i in range(nb):
			h1.Fill(x[i], y[i])
		h1.Draw("hist")
		if inSi:
			h1.GetXaxis().SetTitle(r"z_{Si} [mm]")
		else:
			h1.GetXaxis().SetTitle(r"Stage z [mm]")
		try:
			h1.GetYaxis().SetTitle(f"Charge ({int(charge_in_x_ns)}ns) [a.u.]")
		except:
			h1.GetYaxis().SetTitle(f"Charge ({charge_in_x_ns}ns) [a.u.]")
		h1.Fit(fit_func.GetName(), "WW R")
		fit = h1.GetFunction(fit_func.GetName())
		params = []
		for i in range(5):
			try:
				params.append(fit.GetParameter(i))
			except:
				break
		thickness.append(fit.GetParameter(1) * 1e3) #  convert to um
		rayleigh.append(fit.GetParameter(3) * 1e3)

		fit_func.SetParameters(*params)
		if irrad:
			fit_func.SetParNames("const", "d", "zoff", "z_0", "SPA offset")
		else:
			fit_func.SetParNames("const", "d", "zoff", "z_0")
		fit_func.Draw("same")
		rt.gStyle.SetOptFit(1)
		rt.gPad.SetGrid(1)
		rt.gPad.Update()

		try:
			Vbias = Vbias.split("==")[1]
		except:
			pass
		if output_appendix:
			c1.SaveAs(f"{save_dir}/{measurement_name}_z_fit_{int(Vbias)}V_{output_appendix}.png")
			c1.SaveAs(f"{save_dir}/{measurement_name}_z_fit_{int(Vbias)}V_{output_appendix}.root")
		else:
			c1.SaveAs(f"{save_dir}/{measurement_name}_z_fit_{int(Vbias)}V.png")
			c1.SaveAs(f"{save_dir}/{measurement_name}_z_fit_{int(Vbias)}V.root")

	c1.Close()
	utils.SetMatplotlibParams()
	import matplotlib.pyplot as plt
	plt.clf()
	if na == 0.7:
		thickness = utils.ScaleZ(0.89, np.array(thickness))
		thickness_derivative = utils.ScaleZ(0.89, np.array(thickness_derivative))
		rayleigh = utils.ScaleZ(0.89, np.array(rayleigh))
	elif na == 0.5:
		thickness = utils.ScaleZ(1.3, np.array(thickness))
		thickness_derivative = utils.ScaleZ(1.3, np.array(thickness_derivative))
		rayleigh = utils.ScaleZ(1.3, np.array(rayleigh))
	else:
		sys.exit(f"NA = {na} not available. Plot for depleted region can not be calculated.")
	# voltages_mask = np.array(voltages)<=50
	# voltages = np.array(voltages)[voltages_mask]
	# thickness = np.array(thickness)[voltages_mask]
	# rayleigh = np.array(rayleigh)[voltages_mask]
	fig = plt.figure()
	ax1 = fig.add_subplot(111)
	ax2 = ax1.twinx()
	ax2.set_ylim([np.floor(min(rayleigh)), np.ceil(max(rayleigh))])
	ax2.tick_params(axis='y', colors='r')
	ax2.spines['right'].set_color('r')
	ax2.plot(voltages, rayleigh, color="r", linestyle="--")
	ax2.plot(voltages, rayleigh, ".", color="r", label=r"$z_0$")
	ax1.plot(voltages, thickness, color="k", linestyle="--")
	ax1.plot(voltages, thickness, ".", color="k", label=r"$d_{Fit}$")
	ax1.plot(voltages, thickness_derivative, color="b", linestyle="--")
	ax1.plot(voltages, thickness_derivative, ".", color="b", label=r"$d_{IP}$")
	ax1.set_xlabel("Bias voltages [V]")
	ax1.set_ylabel("Depleted thickness [µm]")
	ax2.set_ylabel("Rayleigh length [µm]")
	ax2.yaxis.label.set_color('r')
	ax1.grid()
	handles_ax1, labels_ax1 = ax1.get_legend_handles_labels()
	handles_ax2, labels_ax2 = ax2.get_legend_handles_labels()
	ax2.legend(handles_ax1 + handles_ax2, labels_ax1 + labels_ax2)
	plt.tight_layout()
	plt.savefig(f"{save_dir}/{measurement_name}_depleted_region_rayleigh_length.png")
	plt.clf()
	fig = plt.figure()
	ax1 = fig.add_subplot(111)
	ax1.plot(voltages, thickness, color="k", linestyle="--")
	ax1.plot(voltages, thickness, ".", color="k", label=r"$d_{Fit}$")
	ax1.plot(voltages, thickness_derivative, color="b", linestyle="--")
	ax1.plot(voltages, thickness_derivative, ".", color="b", label=r"$d_{IP}$")
	ax1.set_ylim(0, thickness[-1] + 20)
	ax1.set_xlabel("Bias voltages [V]")
	ax1.set_ylabel("Depleted thickness [µm]")
	ax1.grid()
	plt.legend()
	plt.tight_layout()
	plt.savefig(f"{save_dir}/{measurement_name}_depleted_region.png")

if __name__ == '__main__':
	main()
