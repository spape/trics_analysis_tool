"""
Script to make a tilt correction for the TPA-TCT setup at CERN.
This script analises data from z scans at different x and y positions.
To obtain a significant z value in the charge profil, the z point for which the TPA charge reaches
the half of its maximum is used.
The obtained xyz points are fitted to a plane, which finally yield the tilt angles in x and y.
Developer: Sebastian Pape
E-Mail: sebastian.pape@cern.ch


This script is executable from the terminal with 'trics_TiltCorrection'.
Required arguments for the parser:
    filename: name of the root file
Optional arguments for the parser:
	-q: Use another quantity instead of charge to obtain the z value from
    -plot: Create a plot of the fitted surface (default True)
    -plot_qz: Plot the QZ point for every XY value (default False)
    -exclude: Exclude given XY points from the plot (Example: -exclude "X1 Y1 X2 Y2" will exclude (X1,Y1) and (X2,Y2) from the fit)
    -hm: Choose which HM should be used for the fit (first: 1, second: 2, default: 1)
    -o: appendix for the output file
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)
"""

import argparse
import os
import sys
import ROOT as rt
import numpy as np
import scipy.linalg
from uncertainties import ufloat
from uncertainties import unumpy as unp
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.optimize import curve_fit
from trics_analysis_scripts import utils

parser = argparse.ArgumentParser()
parser.add_argument('filename', help='name of the root file')
parser.add_argument('-q', help='Use another quantity instead of charge to obtain the z value from', type=str)
parser.add_argument('-plot', help='Create a plot of the fitted surface (default True)', type=str, default=True)
parser.add_argument('-plot_qz', help='Plot the QZ point for every XY value (default False)', type=str, default=False)
parser.add_argument('-exclude',
                    help='Exclude given XY points from the plot (Example: -exclude "X1 Y1 X2 Y2" will exclude (X1,Y1) and (X2,Y2) from the fit, order them that X1>X2 for best performance)',
                    type=str)
parser.add_argument('-fwhm', help='Choose which HM should be used for the fit (first: 1, second: 2, default: 1)', type=int, default=1)
parser.add_argument('-o', help='appendix for the output file', default=False)
parser.add_argument('-tcoll',
                    help='Time in ns for which the charge should be collected (default is 10ns)', type=float,
                    default=10)

"""
Work around the conflicts between pyroot backend and matplotlib backend
"""
rt.gROOT.SetBatch(True)


def main():
	"""
	Get all arguments from the parser.
	"""
	args = parser.parse_args()
	filename = args.filename
	other_quantity = args.q
	measurement_name = filename.split(".")[0]
	output_appendix = args.o
	hm = args.fwhm
	tcoll = args.tcoll
	if int(tcoll) == tcoll:
		tcoll = int(tcoll)
	create_plot = utils.str2bool(args.plot)
	plot_qz = utils.str2bool(args.plot_qz)
	exclude = args.exclude
	if not exclude == None:
		exclude = exclude.split(" ")
		for idx, i in enumerate(exclude):
			exclude[idx] = float(i)
		x_exclude = exclude[::2]
		y_exclude = exclude[1::2]
		if not len(x_exclude) == len(y_exclude):
			sys.exit("Wrong input for -exclude flag.\nX and Y values need to have the same size.")
	else:
		y_exclude = [None]
		x_exclude = [None]
	"""
	Create a plot folder, if it not exist. Only when create_plot is True.
	"""
	if create_plot:
		if not os.path.isdir("plots"):
			os.mkdir("plots")

	"""
	Get root file and TTree.
	"""
	f = rt.TFile(filename)
	tree = f.Get("ch0")

	"""
	Get rounded X and Y values, that are set for the measurement.
	"""
	XY = utils.GetBranchAsArray(filename, "ch0", ["x", "y"])
	X = XY[0]
	Y = XY[1]

	X_Set = utils.GetRoundedValues(X, round_to=3)
	Y_Set = utils.GetRoundedValues(Y, round_to=3)

	"""
	Loop over all combinations of x and y values and calculate an estimate for z value,
	that represents the same feature in every Q(z) graph.
	As a feature the half maximum of the TPA signal is used.
	The closest point to the half maximum and its 4 nearest neighbour points are used
	for a linear fit, to get a better estimation of the real z point.
	"""
	X_points = []
	Y_points = []
	Z_points = []
	for x in X_Set:
		for y in Y_Set:
			"""
			Convert x and y values, because this is more stable with the ROOT TMath::Abs
			"""
			x = float(x)
			y = float(y)
			if y in y_exclude and x in x_exclude:
				index_y = y_exclude.index(y)
				index_x = x_exclude.index(x)
				if index_x == index_y:  ## only exclude the given point tuple
					y_exclude.pop(index_y)
					x_exclude.pop(index_x)  ## delete the point from the exclude list, to avoid double namings
					continue
			x = f"{int(x * 1e3)}e-3"
			y = f"{int(y * 1e3)}e-3"

			if not other_quantity == None:
				tree.Draw(f"{other_quantity}:z",
				          f"TMath::Abs(x-{x})<1e-3 && TMath::Abs(y-{y})<1e-3", "goff")
			else:
				tree.Draw(f"-Sum$((volt-BlineMean) * (time-tleft>0 && time-tleft<{tcoll})):z",
				          f"TMath::Abs(x-{x})<1e-3 && TMath::Abs(y-{y})<1e-3", "goff")

			graph = rt.TGraph(tree.GetSelectedRows(), tree.GetV2(), tree.GetV1())
			try:
				minimum, idx_min, maximum, idx_max = utils.GetMinMax(graph)

				x = float(x)
				y = float(y)

				if abs(minimum) > abs(maximum):
					tpa_half_max = (minimum - maximum) / 2 + maximum
				else:
					tpa_half_max = (maximum - minimum) / 2 + minimum

				if hm == 1:
					closest_z, closest_z_idx = utils.GetFirstHalfTPAMax(graph)
				else:
					closest_z, closest_z_idx = utils.GetSecondHalfTPAMax(graph)

				z_fit = list(graph.GetX())[closest_z_idx - 2: closest_z_idx + 1]
				charge_fit = list(graph.GetY())[closest_z_idx - 2: closest_z_idx + 1]

				params, covariance = curve_fit(utils.linear, z_fit, charge_fit)
				slope = params[0]
				offset = params[1]

				z = (tpa_half_max - offset) / slope

				X_points.append([x])
				Y_points.append([y])
				Z_points.append([z])
				if plot_qz:
					save_dir = f"plots/TiltCorrectionQZ_{measurement_name}"
					c1 = rt.TCanvas("c1", "c1")
					graph.SetTitle(f"X = {x} mm, Y = {y} mm")
					graph.SetLineWidth(3)
					if not os.path.isdir(save_dir):
						os.mkdir(save_dir)
					QZ = rt.TMarker(z, tpa_half_max, 20)
					QZ.SetMarkerColor(2)
					if not other_quantity == None:
						graph.GetYaxis().SetTitle(f"{other_quantity} [a.u.]")
					else:
						graph.GetYaxis().SetTitle(f"Charge ({tcoll}ns) [a.u.]")
					graph.GetXaxis().SetTitle("Stage z [mm]")
					graph.Draw()
					QZ.Draw()
					rt.gPad.SetGrid(1)
					rt.gPad.Update()
					c1.SaveAs(f"{save_dir}/{measurement_name}_x{x}_y{y}.png")
					c1.Close()
			except Exception as e:
				print(f"Error: {e}")
				print(f"\nFit for x:{x} and y:{y} failed.\nContinue\n")
	"""
	Get the x,y and z points in a matrix format and fir a plane to it.
	The plane parameters C1 and C2 are used to calculate the tilted angles u and v.
	"""
	measured_matrix = np.hstack([X_points, Y_points, Z_points])

	def plane(XY, C1, C2, C3):
		X, Y = XY[0, :], XY[1, :]
		return C1 * np.array(X) + C2 * np.array(Y) + C3

	# C, _, _, _ = scipy.linalg.lstsq(A, measured_matrix[:, 2])
	X_points = [i[0] for i in X_points]
	Y_points = [i[0] for i in Y_points]
	Z_points = [i[0] for i in Z_points]
	C, covariance = scipy.optimize.curve_fit(plane, (X_points, Y_points), Z_points)
	C_err = np.sqrt(np.diag(covariance))

	print('\n\n', '*' * 20, ' OUTPUT ', '*' * 20)
	print(f'Found XYZ values:\n{measured_matrix}')
	print("Fit Function:\n\tZ = C1 * X + C2 * Y + C3")

	C1 = ufloat(C[0], C_err[0])
	C2 = ufloat(C[1], C_err[1])
	C3 = ufloat(C[2], C_err[2])
	print(f'Parameters:\n\tC1: {C1}, C2: {C2}±, C3 = {C3}')

	alpha_x = unp.arctan(C1) * 180 / np.pi
	beta_y = unp.arctan(C2) * 180 / np.pi
	v = alpha_x
	u = - beta_y

	print(f'Tilting angles:\n\talpha_x = {alpha_x} deg, beta_y = {beta_y} deg')
	print(f'Tilt compensation:\n\tu = {u} deg, v = {v} deg')

	"""
	Plot and show the data points and the fitted plane.
	Only if create_plot is True.
	"""
	if create_plot:
		utils.SetMatplotlibParams()
		list_min_max = [abs(min(X)), abs(max(X)), abs(min(Y)), abs(max(Y))]
		additional_space = 0.1 * max(list_min_max) #  additional space around the fitted dots for presetantive reasons
		X_plot, Y_plot = np.meshgrid(np.linspace(min(X) - additional_space, max(X) + additional_space, 21),
		                             np.linspace(min(Y) - additional_space, max(Y) + additional_space, 21))
		Z_plot = C[0] * X_plot + C[1] * Y_plot + C[2]
		plt.clf()

		if other_quantity == None:
			quantity = f"Charge({tcoll})ns"
		else:
			quantity = other_quantity
		plt.title(f"{quantity}, $u$ = {u} deg, $v$ = {v} deg")
		ax = plt.axes(projection='3d')
		"""
		Plot xy surface for z = min(z), to compare how big the tilt in the data is
		"""
		flattend_Z = [j for i in Z_plot for j in i]
		surf = ax.plot_surface(X_plot, Y_plot,
		                       np.reshape(np.ones(len(X_plot) * len(X_plot[0])) * min(flattend_Z), np.shape(X_plot)),
		                       rstride=1, cstride=1, alpha=0.8)
		"""
		Plot the plane with measured data
		"""
		surf = ax.plot_surface(X_plot, Y_plot, Z_plot, rstride=1, cstride=1, alpha=0.5, cmap=cm.coolwarm)
		ax.scatter(measured_matrix[:, 0], measured_matrix[:, 1], measured_matrix[:, 2], c='k', s=100)
		plt.xlabel(r'Stage $x$ [mm]')
		plt.ylabel(r'Stage $y$ [mm]')
		ax.set_zlabel(r'Stage $z$ [mm]')
		ax.view_init(azim=-110, elev=15) #  rotate the angle of view
		ax.axis('tight')
		plt.colorbar(surf, shrink=0.5, aspect=5, label=r'Stage $z$ [mm]')
		if output_appendix:
			plt.savefig(f'plots/{measurement_name}_fitted_plane_{output_appendix}.png')
		else:
			plt.savefig(f'plots/{measurement_name}_fitted_plane.png')
		plt.show()


if __name__ == '__main__':
	main()
