# TRICS analysis tool

This python package is intended to help with the analysis of **TPA-TCT** data from the CERN TCT- setup of the SSD group,
which was processed by the **[TRICS](https://gitlab.cern.ch/SSD/TRICS)** framework. It aims to automise **data
plotting**, **tilt correction**, **z scan fits**, and **knife edge analysis**.

## Get the package runnig

Installation requirenments for the package are `python`(version >=3.6) and `pyroot`. Therefore, a prior ROOT installation is
needed ([Install ROOT](https://root.cern/install/)).
After you have successfully installed ROOT, refer to the following
steps to get the package runnig:

1. Clone the repository `git clone https://gitlab.cern.ch/spape/trics_analysis_tool.git` and open a terminal inside the
   repository directory

2. Install via `pip` or `pip3` by executing `pip3 install -e .`

Now you are good to go, the package is installed, and all its tools can immediately be used from the terminal.

## Package functions

The package includs various tools that are accessable from the terminal:

- `trics_CollectedCharge`: Plot multigraph with the collected charge on the _Y_-Axis
- `trics_DriftVelocity`: Plot multigraph with the drift velocity on the _Y_-Axis
- `trics_Multigraph`: Plot a multigraph that has user defined _X_- & _Y_-Axis
- `trics_2D`: Plot two dimensional data with a color coded third dimension
- `trics_Stability`: Plot a quantity over a long time periode
- `trics_zScan`: Plot zScan data with various _Y_-Axis options (_Z_ scan data needed)
- `trics_KnifeEdge`: Analise data from a knife edge scan
- `trics_TiltCorrection`: Execute a tilt correction (_Z_ scan data for various _XY_ values needed)

Executing one of the commands with the flag `--help` or `-h`, yields a short help text for each flag that can be
passed.

> The scripts can produce output `.png` and `.root` files. These will be stored in a folder named `plots`.
This folder, if non-existing in your current directory, will be created in your current directory, if
> output files are produced, by executing one of the above metioned commands.


# Function descriptions

## Scripts to visualise data

The tools to visulaise data, obtained from TPA-TCT measurements, are presented and explained in the following.

### Collected charge

Tool to create charge colletion plots from a TPA-TCT measurement, taken with the TCT- setup at CERN and analized with
the TRICS framework.

This tool is executable from the terminal with `trics_CollectedCharge`.

**Required arguments:**

    filename: name of the root file

**Optional flags:**

    -v: voltages as -v start -v stop -v steps
    -o: appendix for the output file
    -x: Quantity to plot on the x-axis (default z)
    -y: Quantity to plot on the y-axis (default Charge)
    -c: Condition for the plot
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)
    -tleft: Choose between tleft and atleft (default tleft)
    -type: n or p type DUT (default is p type)
    -norm: norm by reference laser power, only available for Charge and VD (default is False)
    -inSi: Multiply x-axis quantity with a hard coded factor that converts it from "in air" to "in silicon" (default is False)

**Example:**

    trics_CollectedCharge 2021_04_16_19_32_39_7859-WL-A63-PIN4.txt.root

**Output file:**

> **Example for a charge collection plot**

<img src="img_readme/2021_04_16_19_32_39_7859-WL-A63-PIN4_CollectedCharge.png" alt="Example charge collection"/>

### Drift velocity

Tool to create drift velocity plots from a TPA-TCT measurement, taken with the TCT- setup at CERN and analized with the
TRICS framework.

This tool is executable from the terminal with `trics_DriftVelocity`.

**Required arguments:**

    filename: name of the root file

**Optional flags:**

    -v: voltages as -v start -v stop -v steps
    -o: appendix for the output file
    -x: Quantity to plot on the x-axis (default z)
    -y: Quantity to plot on the y-axis (default VD)
    -c: Condition for the plot
    -tleft: Choose between tleft and atleft (default tleft)
    -type: n or p type DUT (default is p type)
    -norm: norm by reference laser power, only available for Charge and VD (default is False)
    -inSi: Multiply x-axis quantity with a hard coded factor that converts it from "in air" to "in silicon" (default is False)

**Example:**

    trics_DriftVelocity 2021_04_16_19_32_39_7859-WL-A63-PIN4.txt.root

**Output file:**

> **Example for a drift veolcity plot**

<img src="img_readme/2021_04_16_19_32_39_7859-WL-A63-PIN4_vDrift_z.png" alt="Example drift velocity"/>

### Draw all other quantities

Tool to create multigraph plots from a TPA-TCT measurement, taken with the TCT- setup at CERN and analized with the
TRICS framework.

This tool is executable from the terminal with `trics_Multigraph`.

**Required arguments:**

    filename: name of the root file

**Optional flags:**

	-chain: Chain files to the input file
	-v: voltages as -v start -v stop -v steps
	-ev: exclude a given voltage
    -o: appendix for the output file
    -x: Quantity to plot on the x-axis
    -y: Quantity to plot on the y-axis (Implemented options: Charge, VD)
    -add_x: Additional term to add to the x_axis
    -c: Condition for the plot
    -tcoll: ONLY FOR CHARGE COLLECTION: Time in ns for which the charge should be collected (default is 10ns)
    -tleft: ONLY FOR CHARGE COLLECTION AND DRIFT VELOCITY: Choose between tleft and atleft (default tleft)
    -type: n or p type DUT (default is p type)
    -norm: norm by reference laser power, only available for Charge and VD (default is False)
    -inSi: Multiply x-axis quantity with a hard coded factor that converts it from "in air" to "in silicon" (default is False)

**Example:**

    trics_Multigraph 2021_04_16_19_32_39_7859-WL-A63-PIN4.txt.root -x z -y "tcoll[4]" -v 100 -v 200 -v 11

**Output file:**

> **Example for a multigraph**

<img src="img_readme/2021_04_16_19_32_39_7859-WL-A63-PIN4_z_tcoll4.png" alt="Example multigraph"/>

### Draw two dimensional data

Tool to create two dimensional plots from a TPA-TCT measurement, taken with the TCT- setup at CERN and analized with the
TRICS framework.

This tool is executable from the terminal with `trics_2D`.

**Required arguments:**

    filename: name of the root file

**Optional flags:**

    -v: voltages as -v start -v stop -v steps
    -ev: exclude a given voltage
    -o: appendix for the output file
    -x: Quantity to plot on the x-axis (default x)
    -y: Quantity to plot on the y-axis (default y)
    -z: Quantity to plot on the z-axis (implemented options: Charge, VD; default Charge)
    -npx: Binning size of x-axis (default length of X value array)
    -npy: Binning size of y-axis (default length of Y value array)
    -c: Condition for the plot
    -tcoll: ONLY FOR CHARGE COLLECTION: Time in ns for which the charge should be collected (default is 10ns)
    -tleft: ONLY FOR CHARGE COLLECTION AND DRIFT VELOCITY: Choose between tleft and atleft (default tleft)
    -type: n or p type DUT (default is p type)
    -norm: norm by reference laser power, only available for Charge and VD (default is False)
    -z_t_off: Offset for the Z axis title (default 1.8)
    -rr: Real aspect ratio of the x and y axises (default False)
    -inSi: Multiply z with a hard coded factor that converts it from "in air" to "in silicon" (default is False)
    -contour: Draw a contour line around 0 (default is False)

**Example:**

    trics_2D 2021_03_08_18_34_36_FZ200P_05_DiodeL_9.txt.root -x x -y z -z Charge -norm True

**Output file:**

> **Example for a 2D Graph**

<img src="img_readme/2021_03_08_18_34_36_FZ200P_05_DiodeL_9_x_z_Charge_200V.png" alt="Example 2D plot"/>

### Stability plots

Script to create plots that are optimized to present a quantity over a long time periode, e.g.:

   - Laser power vs time
   - Temperature vs time

This script is executable from the terminal with `trics_Stability`.

**Required arguments:**

    filename: name of the root file
	Quantity: Quantity of the stability plot (Implemented options: T, LP)

**Optional arguments for the parser:**

	-signal: Plot signal inside the stability plot (default True)
	-type: n or p type DUT (default is p type)

## Tools for analysis

The tools to analyse data, obtained from TPA-TCT measurements, are presented and explained in the following.

### Fit z scans

Tool to fit z-Scans of unirradiated devices with the charge carrier density function developed in  `DOI 10.1109`. Build
to work with data from a TPA-TCT measurement, taken with the TCT- setup at CERN and analized with the TRICS framework.

This tool is executable from the terminal with `trics_zScan`.

**Required arguments:**

    filename: name of the root file
    -na: numerical aperture of the used objective (0.5 or 0.7)

**Optional flags:**

    -z: range of fit -z start -z stop
    -v: voltages as -v start -v stop -v steps
    -o: appendix for the output file
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)
    -irrad: Clarrify if the DUT is irradiated, select True, if irradiated and file is not spa corrected (default False)
    -type: n or p type DUT (default is p type)
    -inSi: Multiply the z values with a hard coded factor that converts it from "in air" to "in silicon" (default is False)

**Example:**

    trics_zScan 2021_06_12_20_02_19_Epi50N_07_DiodeL_3.txt.root -inSi True -type n -tcoll 15

**Output file:**

> **Example fit to a Z scan**

<img src="img_readme/2021_06_12_20_02_19_Epi50N_07_DiodeL_3_z_fit_-50V.png" alt="Example zscan fit"/>

> **Example depletion region vs bias voltage plot**

<img src="img_readme/2021_06_12_20_02_19_Epi50N_07_DiodeL_3_depleted_region.png" alt="Example depleteion plot"/>

### Knife Edge analysis
Script to fit on Knife Edge data (xz Scan) of unirradiated devices with the charge carrier density function developed in
`DOI 10.1109`.
Build to work with data from a TPA-TCT measurement, taken with the TCT- setup at CERN and
analized with the TRICS framework.


This script is executable from the terminal with `trics_KnifeEdge`.

**Required arguments:**

    filename: name of the root file
    -x: range of fit -x start -x stop
    -iz: iz position for knife edge fit

**Optional flags:**

    -v: voltages as -v start -v stop -v steps
    -o: appendix for the output file
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)
    -c: Condition for the plot
    -type: n or p type DUT (default is p type)
    -plot: Create a plot for every fit of Q(x,z) (default False)

**Example:**

    trics_KnifeEdge 2021_06_12_12_48_55_Epi50N_07_DiodeL_3.txt.root -x -0.34 -x -0.3 -iz 5 -iz 23 -plot True -type n

**Output file:**

> **Example fit to a Knife Edge data**

<img src="img_readme/2021_06_12_12_48_55_Epi50N_07_DiodeL_3_x_fit_iz16_-50V.png" alt="Example knife edge fit"/>

> **Example beam waist for different Z values**

<img src="img_readme/2021_06_12_12_48_55_Epi50N_07_DiodeL_3_w_z_plot.png" alt="Example w(z) plot"/>

> **Example beam waist for different iZ values**

<img src="img_readme/2021_06_12_12_48_55_Epi50N_07_DiodeL_3_w_z_plot_iz.png" alt="Example w(iz) plot"/>

### Tilt correction

Tool to make a tilt correction for the TPA-TCT setup at CERN. This script analises data from z scans at different x and
y positions. To obtain a significant z value in the charge profil, the z point for which the TPA charge reaches the half
of its maximum is used. The obtained xyz points are fitted to a plane, which finally yield the tilt angles in x and y.

This tool is executable from the terminal with `trics_TiltCorrection`.

**Required arguments:**

    filename: name of the root file

**Optional flags:**

	-q: Use another quantity instead of charge to obtain the z value from
    -plot: Create a plot of the fitted surface (default True)
    -plot_qz: Plot the QZ point for every XY value (default False)
    -exclude: Exclude given XY points from the plot (Example: -exclude "X1 Y1 X2 Y2" will exclude (X1,Y1) and (X2,Y2) from the fit, order them that X1>X2 for best performance)
    -hm: Choose which HM should be used for the fit (first: 1, second: 2, default: 1)
    -o: appendix for the output file
    -tcoll: Time in ns for which the charge should be collected (default is 10ns)

**Example:**

    trics_TiltCorrection 2021_06_11_16_09_53_Epi50N_07_DiodeL_3.txt.root -plot_qz True
The above command will execute a tilt correction analysis using the data of the file `2021_04_16_16_11_55_7859-WL-A63-PIN4.txt.root`.
The flag `-plot_qz True` will create a folder inside the `plots` folder, where `.png's` of the fitted Q(z) plots
are stored.

**Output file:**
> **Example for a tilt correction plot**

<img src="img_readme/2021_06_11_16_09_53_Epi50N_07_DiodeL_3_fitted_plane.png" alt="Example fitted plane"/>

> **Example for a `QZ` plot**

<img src="img_readme/2021_06_11_16_09_53_Epi50N_07_DiodeL_3_x1.15_y1.4.png" alt="Example qz plot"/>
